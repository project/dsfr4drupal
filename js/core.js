/**
 * @file
 * Manage core behaviors.
 */

((Drupal, drupalSettings) => {

  window.dsfr = {
    production: drupalSettings.dsfr4drupal.production,
    verbose: drupalSettings.dsfr4drupal.verbose,
    level: drupalSettings.dsfr4drupal.level,
  };

  Drupal.behaviors.dsfrCore = {
    attach: (context) => {
      // Search all ordered list with "start" attribute.
      context.querySelectorAll("ol[start]").forEach((list) => {
        const start = parseInt(list.getAttribute("start"));

        if (Number.isInteger(start)) {
          // Reset counter with "start" attribute value.
          list.style.counterReset = "li-counter " + (start - 1);
        }
      });
    }
  };

})(Drupal, drupalSettings);
